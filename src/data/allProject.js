export const myProject = [
  {
    name: "Chrome Extension React",
    linkGit: "https://gitlab.com/LeonardoDaFonsecaEsteves/chrome-extension-react",
    description: "Une extension chrome en React.js qui donne la température",
    techno: "React.js",
    linkEnable: true,
    link: "/chrome-extension-react"
  },
  {
    name: "Todo list",
    linkGit: "https://gitlab.com/LeonardoDaFonsecaEsteves/todo-list-react",
    description: "Une Todo liste utilisent React.js et Redux",
    techno: "React.js, Redux",
    linkEnable: true,
    link: "/todo-list"
  },
  {
    name: "SendMail Template",
    linkGit: "https://gitlab.com/LeonardoDaFonsecaEsteves/sendmailtemplate",
    description: "Un outils d'envoi d'email avec un template",
    techno: "React.js, Electron, NodeJS",
    linkEnable: false,
    link: "/todo-list"
  },
  {
    name: "SAM ",
    linkGit: "https://gitlab.com/LeonardoDaFonsecaEsteves/sam",
    description:
      "Aplication en ReactNative pour le tirage au sort du capitain de soirée",
    techno: "ReactNative.js, Expo-cli",
    linkEnable: false,
    link: "/sam"
  }
]
