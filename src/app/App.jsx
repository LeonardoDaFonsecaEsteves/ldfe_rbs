import React from "react"
import logo from "../assets/img/ldfe.png"
import { myProject } from "../data/allProject"
import "../styles/App.scss"
function App() {
  return (
    <div className="App ">
      <header className="App-header container-fluid">
        <div className="row">
          <div className="col-sm">
            <h1>Leonardo Da Fonseca Esteves</h1>
          </div>
          <div className="col-sm">
            <img className="App-logo" src={logo} alt="ldfe logo" />
          </div>
        </div>
      </header>
      <div className="App-link">
        <a target="blank" href="https://gitlab.com/dashboard/projects">
          <h1>Tout mes projects sur GitLab</h1>
        </a>
      </div>
      <div className="container">
        <div className="row">
          {myProject.map((value, key) => {
            return (
              <div
                key={key}
                className="card col-sm"
                style={{ width: "18rem", margin: "5px" }}
              >
                <div className="card-body">
                  <h5 className="card-title">{value.name}</h5>
                  <p className="card-text">{value.description}</p>
                  <h6 class="card-subtitle mb-2 text-muted">{value.techno}</h6>
                  <a
                    target="blank"
                    rel="noopener"
                    href={value.linkGit}
                    className="card-link"
                  >
                    Code Source
                  </a>
                  {value.linkEnable && (
                    <a
                      target="blank"
                      rel="noopener"
                      href={value.link}
                      className="card-link"
                    >
                      Lien démo
                    </a>
                  )}
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default App
